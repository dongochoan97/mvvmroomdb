package com.example.mvvm_roomdatabase.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.anubhav87.mvvmtutorial.repository.NoteRepository
import com.example.mvvm_roomdatabase.model.Note


class NoteViewModel(application: Application) : AndroidViewModel(application) {
    private var repository: NoteRepository = NoteRepository(application)
    private var allNotes: LiveData<List<Note>> = repository.getAllNotes()

    fun insert(note: Note) {
        repository.insert(note)
    }

    fun deleteNote(note: Note){
        repository.deleteNote(note)
    }

    fun deleteAllNotes() {
        repository.deleteAllNotes()
    }
    fun updatelNotes() {
        repository.updateNotes()
    }

    fun getAllNotes(): LiveData<List<Note>> {
        return allNotes
    }
}