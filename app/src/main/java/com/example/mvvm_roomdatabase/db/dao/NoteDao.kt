package com.example.mvvm_roomdatabase.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.mvvm_roomdatabase.model.Note


@Dao
interface NoteDao {

    @Insert
    fun insert(note: Note)

    @Query("DELETE FROM notes_table")
    fun deleteAllNotes()

    @Delete
    fun deleteNote(note: Note)

    @Update
    fun updateNote(note: Note)

    @Query("SELECT * FROM notes_table ")
    fun getAllNotes(): LiveData<List<Note>>

}