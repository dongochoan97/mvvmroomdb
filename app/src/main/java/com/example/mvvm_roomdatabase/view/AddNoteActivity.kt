package com.example.mvvm_roomdatabase.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.mvvm_roomdatabase.R
import kotlinx.android.synthetic.main.add_note_activity.*

class AddNoteActivity:AppCompatActivity() {

    companion object {
        const val EXTRA_TITLE = "com.example.mvvm_roomdatabase.EXTRA_TITLE"
        const val EXTRA_CONTENT = "com.example.mvvm_roomdatabase.EXTRA_CONTENT"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_note_activity)
        btnAdd.setOnClickListener {
            saveNote()
        }
    }

    private fun saveNote() {
        if (edtTittle.text.toString().trim().isBlank() || edtContent.text.toString().trim().isBlank()) {
            Toast.makeText(this, "Nhập đầy đủ thông tin !", Toast.LENGTH_SHORT).show()
            return
        }

        val data = Intent().apply {
            putExtra(EXTRA_TITLE, edtTittle.text.toString())
            putExtra(EXTRA_CONTENT, edtContent.text.toString())
        }

        setResult(Activity.RESULT_OK, data)
        finish()
    }
}