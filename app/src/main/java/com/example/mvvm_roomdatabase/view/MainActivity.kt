package com.example.mvvm_roomdatabase.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mvvm_roomdatabase.model.Note
import com.example.mvvm_roomdatabase.viewmodel.NoteViewModel

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog
import com.example.mvvm_roomdatabase.R


class MainActivity : AppCompatActivity() {

    private lateinit var noteViewModel: NoteViewModel
    private lateinit var adapter: NoteAdapter
    private val ADD_NOTE_REQUEST = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        noteViewModel = ViewModelProviders.of(this).get(NoteViewModel::class.java)
        setupRecyclerView()
        noteViewModel.getAllNotes().observe(this, Observer { listData->
            listData?.let {
                adapter.setNotes(it)
            }
        })
        fab.setOnClickListener { view ->
            startActivityForResult(Intent(this,AddNoteActivity::class.java),ADD_NOTE_REQUEST)
        }
    }

    private fun setupRecyclerView() {
        adapter = NoteAdapter(delete = {
            val dialog = AskOption(it)
            dialog.show()
        })
        recycler_view.layoutManager = LinearLayoutManager(this)
        recycler_view.setHasFixedSize(true)
        recycler_view.adapter = adapter
    }


    private fun AskOption(note: Note): AlertDialog {

        return AlertDialog.Builder(this)
            // set message, title, and icon
            .setTitle("Xoá")
            .setMessage("Bạn có muốn xoá bản ghi này không ?")
            .setIcon(R.drawable.ic_delete)

            .setPositiveButton("Có",
                DialogInterface.OnClickListener { dialog, whichButton ->
                    noteViewModel.deleteNote(note)
                    dialog.dismiss()
                })
            .setNegativeButton("Không",
                DialogInterface.OnClickListener { dialog, which -> dialog.dismiss() })
            .create()
    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_delete_all -> {
                noteViewModel.deleteAllNotes()
                Toast.makeText(this,"Đã xoá tất cả bản ghi",Toast.LENGTH_LONG).show()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ADD_NOTE_REQUEST && resultCode == Activity.RESULT_OK) {
            data?.let {
                val newNote = Note(
                    data.getStringExtra(AddNoteActivity.EXTRA_TITLE),
                    data.getStringExtra(AddNoteActivity.EXTRA_CONTENT)
                )
                noteViewModel.insert(newNote)
                Toast.makeText(this, "Thêm thành công !", Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(this, "Thêm không thành công !", Toast.LENGTH_SHORT).show()
        }
    }
}
